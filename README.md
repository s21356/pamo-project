# FlappySpace Matt

Project implemented as part of the Mobile Applications course. 
In the game, the player takes on the role of Matt Deamon, who, like Brad Pitt in the movie Ad Astra, wants to explore space. 
Basic Functionalities:

- dynamic obstacle avoidance on screen touch
- displaying the score of the current game 

