package com.pamo.flappyspacematt;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Class instantiates BitmapBank, GameEngine, and to initializes all properties like SCREEN_WIDTH,
 * SCREEN_HEIGHT, gravity, VELOCITY_WHEN_JUMPED, numberOfAliens etc using initialization(),
 * setGameConstants(), setScreenSize(Context context) etc. and defines some getter methods
 * as well for getting those objects.
 */
public class AppConstants {
    /**
     * Bitmap object reference
     */
    static BitmapBank bitmapBank;
    /**
     * GameEngine object reference
     */
    static GameEngine gameEngine;

    static int SCREEN_WIDTH, SCREEN_HEIGHT;
    static int gravity;
    static int VELOCITY_WHEN_JUMPED;
    static int gapBetweenTopAndBottomAliens;
    static int numberOfAliens;
    static int alienVelocity;
    static int minAlienOffsetY;
    static int maxAlienOffsetY;
    static int distanceBetweenAliens;
    /**
     * Stores GameActivity
     */
    static Context gameActivityContext;
    /**
     * Method initializing Bitmap and GameEngine objects references
     */
    public static void initialization(Context context) {
        setScreenSize(context);
        bitmapBank = new BitmapBank(context.getResources());
        setGameConstants();
        gameEngine = new GameEngine();
    }

    /**
     * Method setting gravity, velocity when jumped and other game constants
     */
    public static void setGameConstants() {
        AppConstants.gravity = 3;
        AppConstants.VELOCITY_WHEN_JUMPED = -30;
        gapBetweenTopAndBottomAliens = 700;
        AppConstants.numberOfAliens = 2;
        AppConstants.alienVelocity = 17;
        AppConstants.minAlienOffsetY = (int)(AppConstants.gapBetweenTopAndBottomAliens/2.0);
        AppConstants.maxAlienOffsetY = AppConstants.SCREEN_HEIGHT - AppConstants.minAlienOffsetY - AppConstants.gapBetweenTopAndBottomAliens;
        AppConstants.distanceBetweenAliens = SCREEN_WIDTH * 9/10;
    }
/**
 * Method returning BitmapBank instance
 */
    public static BitmapBank getBitmapBank() {
        return bitmapBank;
    }

    /**
     * Method returning GameEngine instance
     */
    public static GameEngine getGameEngine() {
        return gameEngine;
    }

    /**
     * Method setting the screen size (needed to fill whole screen with background image)
     * @param context
     */
    private static void setScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        AppConstants.SCREEN_WIDTH = width;
        AppConstants.SCREEN_HEIGHT = height;
    }

}
