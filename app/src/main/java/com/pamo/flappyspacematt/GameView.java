package com.pamo.flappyspacematt;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

/**
 * Class responsible for creating view of the game and instantiating GameThread object
 * It extends SurfaceView, implements SurfaceHolder.Callback public static interface.
 * From the initView() in constructor, SurfaceHolder and GameThread objects are being instantiated.
 * From the surfaceCreated overridden method the thread is started if it's not running.
 * From surfaceDestroyed method it is destroyed if it's running.
 * isRunning is a Flag to detect whether the thread is running or not.
 * GameView also overrides onTouchEvent(MotionEvent event) to detect the touch event and sets Rocket
 * velocity when touched.
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    GameThread gameThread;

    public GameView(Context context) {
        super(context);
        initView();
    }

    /**
     * Starts the thread  if it's not running.
     * @param holder
     */
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        if(!gameThread.isRunning()) {
            gameThread = new GameThread(holder);
            gameThread.start();
        } else {
            gameThread.start();
        }
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {
    }

    /**
     * Destroys the thread if it is running.
     * @param surfaceHolder
     */
    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
        if (gameThread.isRunning()) {
            gameThread.setIsRunning(false);
            boolean retry = true;
            while(retry) {
                try {
                    gameThread.join();
                    retry = false;
                } catch(InterruptedException e){}
            }
        }
    }

    /**
     * Method initializing SurfaceHolder and thread
     */
    void initView() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setFocusable(true);
        gameThread = new GameThread(holder);
    }

    /**
     * Method detecting tapping the screen setting gamestate and setting velocity of the rocket
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    int action = event.getAction();
    /**
     *Tap is detected
     * */
    if (action == MotionEvent.ACTION_DOWN){
        AppConstants.getGameEngine().gameState = 1;
        AppConstants.getGameEngine().rocket.setVelocity(AppConstants.VELOCITY_WHEN_JUMPED);
    }
    return true;
    }
}