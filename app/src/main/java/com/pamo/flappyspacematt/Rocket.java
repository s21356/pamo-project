package com.pamo.flappyspacematt;

/**
 * Class responsible for X and Y racket coordinates,
 */
public class Rocket {

    private int rocketX, rocketY, velocity;
    /**
     * Constructor of rocket image. Creates rocket and sets its position in the middle of the screen
     *
     */
    public Rocket(){
        rocketX = AppConstants.SCREEN_WIDTH/2 - AppConstants.getBitmapBank().getRocketWidth()/2;
        rocketY = AppConstants.SCREEN_HEIGHT/2 - AppConstants.getBitmapBank().getRocketHeight()/2;
        velocity = 0;
    }

    /**
     * @return velocity
     */
    public int getVelocity(){
        return velocity;
    }

    /**
     * Setter method for setting the velocity
     */
    public void setVelocity(int velocity){
        this.velocity=velocity;
    }

    /**
     * Getter method for getting the rocketX
     */
    public int getX(){
        return rocketX;
    }
    /**
     * Getter method for getting the rocketY
     */
    public int getY(){
        return rocketY;
    }
    /**
     * Setter method for setting the Y-coordinate of rocket
     */
    public void setX (int rocketX){
        this.rocketX = rocketX;
    }
    /**
     * Setter method for setting the Y-coordinate of rocket
     */
    public void setY (int rocketY){
        this.rocketY = rocketY;
    }
}
