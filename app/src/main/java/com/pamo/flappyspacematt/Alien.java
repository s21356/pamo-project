package com.pamo.flappyspacematt;

import java.util.Random;

/**
 * Alien class is responsible for obstacles(aliens)
 */
public class Alien {
    /**
     * alienX -> Alien X-coordinate
     * topAlienOffsetY -> top alien bottom edge coordinate
     */
    private int alienX, topAlienOffsetY;
    private Random random;

    public Alien(int alienX, int topAlienOffsetY) {
        this.alienX = alienX;
        this.topAlienOffsetY = topAlienOffsetY;
        random = new Random();
    }

    /**
     *
     * @return Top alien bottom Y coordinate
     */
    public int getTopAlienOffsetY() {
        return topAlienOffsetY;
    }

    public int getAlienX() {
        return alienX;
    }

    /**
     *
     * @return Top alien top Y coordinate (ceiling)
     */
    public int getTopAlienY() {
        return topAlienOffsetY - AppConstants.getBitmapBank().getAlienHeight();
    }

    /**
     *
     * @return Bottom alien top Y coordinate
     */
    public int getBottomAlienY() {
        return topAlienOffsetY + AppConstants.gapBetweenTopAndBottomAliens;
    }

    public void setAlienX(int alienX) {
        this.alienX = alienX;
    }

    public void setTopAlienOffsetY(int topAlienOffsetY) {
        this.topAlienOffsetY = topAlienOffsetY;
    }
}
