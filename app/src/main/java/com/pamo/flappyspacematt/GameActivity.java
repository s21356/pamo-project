package com.pamo.flappyspacematt;
/**
 * FlappySpaceMatt - game where player can feel like Matt facing aliens on dangerous planet.
 * Authors: Michał Czerwiak, Katarzyna Węsierska
 * GameActivity class instantiates GameView object and sets the view with that.
 */
import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

public class GameActivity extends Activity {

    GameView gameView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppConstants.gameActivityContext = this;
        /**
         * Object used to det the content view
         */
        gameView = new GameView(this);
        setContentView(gameView);
    }

}