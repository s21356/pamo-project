package com.pamo.flappyspacematt;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class GameOver extends AppCompatActivity {
    TextView tvScore;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over);
        /**Receiving score from intent (in GameEngine)
         *
         */
        int score = getIntent().getExtras().getInt("score");
        tvScore = findViewById(R.id.tvScore);
        tvScore.setText("" + score);
    }

    /**
     * Restarts the Game
     * @param view
     */
    public void restart(View view) {
        Intent intent = new Intent(GameOver.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Finishes the game
     * @param view
     */
    public void exit(View view) {
        Intent intent = new Intent(GameOver.this, MainActivity.class);
        finish();
    }

}
