package com.pamo.flappyspacematt;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * This class instantiates SurfaceHolder object.
 * Inside the run method of the thread, till the isRunning boolean variable is true, it continuously
 * locks the canvas, gets the gameEngine object from AppConstants, calls 3 methods to draw and update
 * backgroundImage, Rocket and Aliens on Canvas using updateAndDrawBackgroundImage(canvas),
 * drawRocket(canvas) and updateAndDrawAliens(canvas) respectively and then calls unlockCanvasAndPost()
 * method to finish editing pixels in the surface.
 * SurfaceHolder is a public Interface.
 */
public class GameThread extends Thread {

    /**
     * Surfaceholder object reference
     */
    SurfaceHolder surfaceHolder;
    /**
     * isRunning determines whether the thread is running or not
     */
    boolean isRunning;
    /**
     * Loop start time and loop duration
     */
    long startTime, loopTime;
    /**
     * Delay in milliseconds between screen refreshes
     */
    long DELAY = 33;
    public GameThread(SurfaceHolder surfaceHolder) {
        this.surfaceHolder = surfaceHolder;
        isRunning = true;
    }

    /**
     * Run method is running in a loop as long as isRunning parameter is true
     * This method starts when start() method is called in GameView class on GameThread object.
     * startTime variable stores currrent time
     */
    @Override
    public void run() {
        while(isRunning) {
            startTime = SystemClock.uptimeMillis();
            /**
             * Locking the canvas
             */
            Canvas canvas = surfaceHolder.lockCanvas(null);
            if(canvas != null) {
                /**
                 * Synchronized block responsible for updating and drawing background, rocket and
                 * obstacles
                 */
                synchronized (surfaceHolder) {
                    AppConstants.getGameEngine().updateAndDrawBackgroundImage(canvas);
                    AppConstants.getGameEngine().drawRocket(canvas);
                    AppConstants.getGameEngine().updateAndDrawAliens(canvas);
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            /**
             * Sleep time is needed for steady animation
             */
            loopTime = SystemClock.uptimeMillis() - startTime;
            if(loopTime < DELAY) {
                try {
                    Thread.sleep(DELAY - loopTime);
                } catch(InterruptedException e) {
                    Log.e("Interrupted", "Interrupted while sleeping");
                }
            }
        }
    }

    /**
     * Method returning whether the thread is running
     */
    public boolean isRunning() {
        return isRunning;
    }
    /**
     * Method setting the thread state, false = stopped, true = running
     */
    public void setIsRunning(boolean state) {
        isRunning = state;
    }

}