package com.pamo.flappyspacematt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class has all business logic and drawing methods.
 * It keeps instances of all displayed objects (background image, Matt's racket, ArrayList of aliens,
 * scoring system.
 * This class also defines various methods to update and draw Aliens, BackgroundImage, Rocket etc.
 */
public class GameEngine {

    BackgroundImage backgroundImage;
    Rocket rocket;
    static int gameState;
    ArrayList<Alien> aliens;
    Random random;
    int score;
    int scoringAlien;
    Paint scorePaint;

    public GameEngine() {

        backgroundImage = new BackgroundImage();
        rocket = new Rocket();
        /**
         * gameState indicates what is going on with the game: 0-not started, 1-playing, 2-game over
         */
        gameState = 0;
        aliens = new ArrayList<>();
        random = new Random();
        /**
         * For loop creates two obstacles(aliens). In first iteration i=0 so alienX coordinate will be equal to
         * Screen width. Next obstacle will be separated by 9/10 units of the screen
         * topAlienOffsetY is generated using nextInt() from Random class between minAlienOffsetY and maxAlienOffsetY.
         * Finally it creates Alien object with "beginning" coordinates and adds it to array. Array list is
         * used to draw obstacles in updateAndDrawAliens method.
         */
        for (int i = 0; i < AppConstants.numberOfAliens; i++) {
            int alienX = AppConstants.SCREEN_WIDTH + i * AppConstants.distanceBetweenAliens;
            int topAlienOffsetY = AppConstants.minAlienOffsetY + random.nextInt(AppConstants.maxAlienOffsetY - AppConstants.minAlienOffsetY + 1);
            Alien alien = new Alien(alienX, topAlienOffsetY);
            aliens.add(alien);
        }
        score = 0;
        scoringAlien = 0;

        /**
         * Showing and styling text of the number of points
         */
        scorePaint = new Paint();
        scorePaint.setColor(Color.WHITE);
        scorePaint.setTextSize(100);
        scorePaint.setTextAlign(Paint.Align.CENTER);

    }

    /**
     * Method responsible for drawing the side-scrolling background image
     *
     * @param canvas It decreases X coordinate by the velocity (right-to-left movement)
     *               If X coordinate becomes less than minus background width, it reinitialize it to 0
     */
    public void updateAndDrawBackgroundImage(Canvas canvas) {
        backgroundImage.setX(backgroundImage.getX() - backgroundImage.getVelocity());
        if (backgroundImage.getX() < -AppConstants.getBitmapBank().getBackgroundWidth()) {
            backgroundImage.setX(0);
        }
        canvas.drawBitmap(AppConstants.getBitmapBank().getBackground(), backgroundImage.getX(), backgroundImage.getY(), null);
    }

    /**
     * Method responsible for drawing rocket
     * When game is running it sets velocity of the rocket to the value of velocity
     * plus value of gravity and then sets the Y coordinate of the rocket increased by
     * velocity. This imitates falling rocket to the surface of the planet.
     * @param canvas
     */
    public void drawRocket(Canvas canvas) {
        if (gameState == 1) {
            if (rocket.getY() < (AppConstants.SCREEN_HEIGHT - AppConstants.getBitmapBank().getRocketHeight()) || rocket.getVelocity() < 0) {
                rocket.setVelocity(rocket.getVelocity() + AppConstants.gravity);
                rocket.setY(rocket.getY() + rocket.getVelocity());
            }
        }
        canvas.drawBitmap(AppConstants.getBitmapBank().getRocket(), rocket.getX(), rocket.getY(), null);
    }

    /**
     * Method responsible for drawing obstacles, animating their movement and detecting collision.
     * When the first alien goes away from the left edge of the screen alienX coordinate is set in the 3rd position again.
     * topAlienOffsetY is randomly changed. This way there can be created infinite number of aliens.
     * Movement is done by substracting velocity from alienX coordinate and then setting new alienX
     * coordinate in a for loop.
     * @param canvas
     */
    public void updateAndDrawAliens(Canvas canvas) {
        /**Collision detection = game over
         *
         */
        if (gameState == 1) {
            if ((aliens.get(scoringAlien).getAlienX() < rocket.getX() + AppConstants.getBitmapBank().getRocketWidth())
                    && (aliens.get(scoringAlien).getTopAlienOffsetY() > rocket.getY()
                    || aliens.get(scoringAlien).getBottomAlienY() < (rocket.getY() +
                    AppConstants.getBitmapBank().getRocketHeight()))) {
                /**
                 * Go to game over screen after collision detection
                 */
                gameState = 2;
                Context context = AppConstants.gameActivityContext;
                Intent intent = new Intent(context, GameOver.class);
                intent.putExtra("score", score);
                context.startActivity(intent);
                ((Activity) context).finish();
                /**
                 * Passing by obstacle without collision. Increasing score.
                 */
            } else if (aliens.get(scoringAlien).getAlienX() < rocket.getX() - AppConstants.getBitmapBank().getAlienWidth()) {
                score++;
                scoringAlien++;
                if (scoringAlien > AppConstants.numberOfAliens - 1) {
                    scoringAlien = 0;
                }
            }
            /**
             * Drawing Obstacles
             * When the first alien goes away from the left edge of the screen alienX coordinate is set in the 3rd position again.
             * topAlienOffsetY is randomly changed. This way there can be created infinite number of aliens.
             * Movement is done by substracting velocity from alienX coordinate and then setting new alienX
             * coordinate in a for loop.
             */
            for (int i = 0; i < AppConstants.numberOfAliens; i++) {
                if (aliens.get(i).getAlienX() < -AppConstants.getBitmapBank().getAlienWidth()) {
                    aliens.get(i).setAlienX(aliens.get(i).getAlienX() + AppConstants.numberOfAliens * AppConstants.distanceBetweenAliens);
                    int topAlienOffsetY = AppConstants.minAlienOffsetY + random.nextInt(AppConstants.maxAlienOffsetY - AppConstants.minAlienOffsetY + 4);
                    aliens.get(i).setTopAlienOffsetY(topAlienOffsetY);
                }
                aliens.get(i).setAlienX(aliens.get(i).getAlienX() - AppConstants.alienVelocity);
                canvas.drawBitmap(AppConstants.getBitmapBank().getAlienTop(), aliens.get(i).getAlienX(), aliens.get(i).getTopAlienY(), null);
                canvas.drawBitmap(AppConstants.getBitmapBank().getAlienBottom(), aliens.get(i).getAlienX(), aliens.get(i).getBottomAlienY(), null);
            }
        }
        canvas.drawText("Score: " + score, 500, 110, scorePaint);
    }

}