package com.pamo.flappyspacematt;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Class responsible for handling background, rocket and aliens images
 */
public class BitmapBank {

    Bitmap background;
    Bitmap rocket;
    Bitmap alienTop, alienBottom;

    /**
     * Constructor gets background, rocket and aliens images from resources and creates background bitmap
     *
     * @param res - game resources
     */
    public BitmapBank(Resources res) {
        background = BitmapFactory.decodeResource(res, R.drawable.background);
        background = scaleImage(background);
        rocket = BitmapFactory.decodeResource(res,R.drawable.rocket);
        alienTop = BitmapFactory.decodeResource(res,R.drawable.alien_top);
        alienBottom = BitmapFactory.decodeResource(res,R.drawable.alien_bottom);
    }

    /**
     *
     * @return alien-top bitmap
     */
    public Bitmap getAlienTop() {
        return alienTop;
    }

    /**
     *
     * @return alien-bottom bitmap
     */
    public Bitmap getAlienBottom() {
        return alienBottom;
    }

    /**
     *
     * @return alien width
     */
    public int getAlienWidth() {
        return alienTop.getWidth();
    }


    /**
     *
     * @return alien height
     */
    public int getAlienHeight() {
        return alienTop.getHeight();
    }
    /**
     * @return rocket bitmap
     */
    public Bitmap getRocket(){
        return rocket;
    }
    /**
     * @return rocket width
     */
    public int getRocketWidth(){
        return rocket.getWidth();
    }
    /**
     * @return rocket height
     */
    public int getRocketHeight(){
        return rocket.getHeight();
    }

    public Bitmap getBackground() {
        return background;
    }

    /**
     * @return background width
     */
    public int getBackgroundWidth() {
        return background.getWidth();
    }

    /**
     * @return background height
     */
    public int getBackgroundHeight() {
        return background.getHeight();
    }

    /**
     * Method responsible for scaling background image to full screen.
     *
     * @param bitmap
     * @return
     */
    public Bitmap scaleImage(Bitmap bitmap) {
        float widthHeightRatio = getBackgroundWidth() / getBackgroundHeight();
        int backgroundScaledWidth = (int) widthHeightRatio * AppConstants.SCREEN_HEIGHT;
        return Bitmap.createScaledBitmap(bitmap, backgroundScaledWidth, AppConstants.SCREEN_HEIGHT, false);
    }

}

