package com.pamo.flappyspacematt;
/**
 * FlappySpaceMatt - game where player can feel like Matt facing aliens on dangerous planet.
 * Authors: Michał Czerwiak, Katarzyna Węsierska.
 * MainActivity class calls AppConstants.initialization static method. Starts GameActivity
 * when Play button is tapped.
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * Calling initialization method from AppConstants with game context passed
         */
        AppConstants.initialization(this.getApplicationContext());
    }

    /**
     * Function triggering the game
     * @param view
     */
    public void startGame(View view) {
        /**
         * Intent object acts as glue between two Activities - MainActivity and GameActivity
         */
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        /**
         * finishing MainActivity as it is no longer needed
         */
        finish();
    }
    
}