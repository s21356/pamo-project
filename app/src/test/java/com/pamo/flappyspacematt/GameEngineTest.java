package com.pamo.flappyspacematt;

import android.graphics.Canvas;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameEngineTest {
    private GameEngine gameEngine;
    private Canvas canvas;

    @Before
    public void setUp() {
        gameEngine = new GameEngine();
        canvas = new Canvas();
    }

    @Test
    public void testUpdateAndDrawBackgroundImage() {
        // Test the initial position of the background image
        assertEquals(0, gameEngine.backgroundImage.getX());

        // Update and draw the background image
        gameEngine.updateAndDrawBackgroundImage(canvas);

        // Verify that the position of the background image has changed
        assertNotEquals(0, gameEngine.backgroundImage.getX());
    }

    @Test
    public void testDrawRocket() {
        // Set the game state to 1
        gameEngine.gameState = 1;

        // Set the rocket's initial position and velocity
        gameEngine.rocket.setY(AppConstants.SCREEN_HEIGHT - AppConstants.getBitmapBank().getRocketHeight());
        gameEngine.rocket.setVelocity(-10);

        // Draw the rocket
        gameEngine.drawRocket(canvas);

        // Verify that the rocket's position has changed
        assertNotEquals(AppConstants.SCREEN_HEIGHT - AppConstants.getBitmapBank().getRocketHeight(), gameEngine.rocket.getY());
    }

    @Test
    public void testUpdateAndDrawAliens() {
        // Set the game state to 1
        gameEngine.gameState = 1;

        // Set the alien's initial position
        gameEngine.aliens.get(0).setAlienX(AppConstants.getBitmapBank().getAlienWidth());

        // Update and draw the aliens
        gameEngine.updateAndDrawAliens(canvas);

        // Verify that the alien's position has changed
        assertNotEquals(AppConstants.getBitmapBank().getAlienWidth(), gameEngine.aliens.get(0).getAlienX());
    }
}
